console.log("hello world");


//Arrays

const students = [
    "Tony",
    "Peter",
    "wanda",
    "Vision",
    "Loki"
]
console.log(typeof students);
console.log(students);
students.push("steve");

console.log(students);
students.unshift("Thor");
console.log(students);

const arrNum = [15, 20, 25, 30, 11];


// arrNum.forEach((num)=>{

// 	if(num%5==0){
// 		console.log(num);
// 	}else{
// 		console.log(false);
// 	}
// })

let divisibleBy5 = arrNum.every(num => {
    console.log(num);
    return num % 5 === 0;
})
console.log(divisibleBy5);


//Math
//mathematical constants
//8 pre-defined properties which can be call via the syntax "Math.property"

console.log(Math);
console.log(Math.E); // returns Euler's number
console.log(Math.PI); // returns PI
console.log(Math.SQRT2); // returns the square root of 2
console.log(Math.SQRT1_2); // returns the square root of 1/2
console.log(Math.LN2); // returns the natural logarithm of 2
console.log(Math.LN10); // returns the natural logarithm of 10
console.log(Math.LOG2E); // returns base 2 logarithm of E
console.log(Math.LOG10E); // returns base 10 logarithm of E


// console.log(Array.isArray(arrNum));
// console.log(Math instanceof Array );

//methods 
console.log(Math.round(Math.PI));//returns the nearest integer
console.log(Math.ceil(Math.PI));//returns the value of x rounded up to its nearest integer
console.log(Math.floor(Math.PI));//returns the value of x rounded down to its nearest integer
console.log(Math.trunc(Math.PI));//returns the integer part of x


console.log(Math.sqrt(3.14));//return a square root of a number

//return the lowest value of the list
console.log(Math.min(-1, -2, -4, 0, 1, 2, 3, 4,));
//return the highest value of the list
console.log(Math.max(-1, -2, -4, 0, 1, 2, 3, 4,));
/*
1 How do you create arrays in JS?
2 How do you access the first character of an array?
3 How do you access the last character of an array?
4 What array method searches for, and returns the index of a given value in an array? 
5 This method returns -1 if given value is not found in the array.
What array method loops over all elements of an array, performing a user-defined function on each iteration?

6.   What array method creates a new array with elements obtained from a user-defined function?
7.   What array method checks if all its elements satisfy a given condition?
8.   What array method checks if at least one of its elements satisfies a given condition?
9.   True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
10.   True or False: array.slice() copies elements from original array and returns these as a new array.
*/