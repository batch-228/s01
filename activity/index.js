

//QUIZ

// 1 How do you create arrays in JS?

//ans: varArray=[]

// 2 How do you access the first character of an array?
//ans: varArray[0]

// 3 How do you access the last character of an array?
//ans: varArray[varArray.length - 1]);

// 4 What array method searches for, and returns the index of a given value in an array?
// This method returns -1 if given value is not found in the array.
// ans: .indexOf()

// 5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
//ans: .forEach()

// 6.   What array method creates a new array with elements obtained from a user-defined function?
//ans: .map()

// 7.   What array method checks if all its elements satisfy a given condition?
//ans: .every()

// 8.   What array method checks if at least one of its elements satisfies a given condition?
//ans: .some()

// 9.   True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
//ans: False

// 10.   True or False: array.slice() copies elements from original array and returns these as a new array.
//ans: True




// FUNCTION CODING

let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students);

// 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.

function addToEnd(students, name) {

    if (!Array.isArray(students)) {
        return "error - 1st argument is not an array";
    }

    if (typeof name === "string" && name.length !== 0) {
        students.push(name);
        return students;
    }

    return "error - can only add strings to an array";
}

console.log(addToEnd(students, "Ryan"));

// 2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.

function addToStart(students, name) {

    if (!Array.isArray(students)) {
        return "error - 1st argument is not an array";
    }

    if (typeof name === "string" && name.length !== 0) {
        students.unshift(name);
        return students;
    }

    return "error - can only add strings to an array";
}

console.log(addToStart(students, "Tess"));

// 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

function elementChecker(students, name) {

    if (!Array.isArray(students)) {
        return "error - 1st argument is not an array";
    }

    if (students.length === 0) {
        return "error - passed in array is empty";
    }

    if (name.length === 0) {
        return false;
    }

    return students.some(elem => elem === name);
}

console.log(elementChecker(students, "Jane"));

// 4. Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:
// if array is empty, return "error - array must NOT be empty"
// if at least one array element is NOT a string, return "error - all array elements must be strings"
// if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
// if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
// if every element in the array ends in the passed in character, return true. Otherwise return false.
// Use the students array and the character "e" as arguments when testing.

function checkAllStringsEnding(students, char) {

    if (!Array.isArray(students)) {
        return "error - 1st argument is not an array";
    }

    if (students.length === 0) {
        return "error - array must NOT be empty";
    }

    let isAllString = students.every(elem => typeof elem === "string");

    if (!isAllString) {
        return "error - all array elements must be strings";
    }

    if (typeof char !== "string") {
        return "error - 2nd argument must be of data type string";
    }

    if (char.length > 1) {
        return "error - 2nd argument must be a single character";
    }

    return students.every(elem => elem[elem.length - 1] === char);
}

console.log(checkAllStringsEnding(students, "e"));



// 5. Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

function stringLengthSorter(students) {

    if (!Array.isArray(students)) {
        return "error - 1st argument is not an array";
    }

    if (students.length === 0) {
        return "error - array must NOT be empty";
    }

    let isAllString = students.every(elem => typeof elem === "string");

    if (!isAllString) {
        return "error - all array elements must be strings";
    }


    return students.sort((x, y) => x.length - y.length);
}

console.log(stringLengthSorter(students));



// 6.  Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:
// if array is empty, return "error - array must NOT be empty"
// if at least one array element is NOT a string, return "error - all array elements must be strings"
// if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
// if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
// return the number of elements in the array that start with the character argument, must be case-insensitive
// Use the students array and the character "J" as arguments when testing.


function startsWithCounter(students, char) {

    if (!Array.isArray(students)) {
        return "error - 1st argument is not an array";
    }

    if (students.length === 0) {
        return "error - array must NOT be empty";
    }

    let isAllString = students.every(elem => typeof elem === "string");

    if (!isAllString) {
        return "error - all array elements must be strings";
    }

    if (typeof char !== "string") {
        return "error - 2nd argument must be of data type string";
    }

    if (char.length > 1) {
        return "error - 2nd argument must be a single character";
    }

    return students.filter(elem => elem[0].toLowerCase() === char.toLowerCase()).length;
}

console.log(startsWithCounter(students, "J"));



// 7.    Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:
// if array is empty, return "error - array must NOT be empty"
// if at least one array element is NOT a string, return "error - all array elements must be strings"
// if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
// return a new array containing all elements of the array argument that have the string argument in it, must be case-insensitive
// Use the students array and the character "jo" as arguments when testing.


function likeFinder(students, char) {

    if (!Array.isArray(students)) {
        return "error - 1st argument is not an array";
    }

    if (students.length === 0) {
        return "error - array must NOT be empty";
    }

    let isAllString = students.every(elem => typeof elem === "string");

    if (!isAllString) {
        return "error - all array elements must be strings";
    }

    if (typeof char !== "string") {
        return "error - 2nd argument must be of data type string";
    }


    return students.filter(elem => elem.toLowerCase().match(char));
}

console.log(likeFinder(students, "jo"));




// 8. Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.

function randomPicker(students) {

    if (!Array.isArray(students)) {
        return "error - 1st argument is not an array";
    }

    return students[Math.floor(Math.random() * students.length)]
}

console.log(randomPicker(students))


console.log(students);